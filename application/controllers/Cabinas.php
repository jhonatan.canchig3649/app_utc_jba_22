<?php
  class Cabinas extends CI_Controller{
      public function __construct(){
        parent::__construct();
        $this->load->model("cabina");

      }
      public function index(){
        $data["listadoCabinas"]=$this->cabina->consultarTodos();
        $this->load->view("header");
        $this->load->view("cabinas/index",$data);
        $this->load->view("footer");
      }
      public function metal(){
        $this->load->view("header");
        $this->load->view("cabinas/metal");
        $this->load->view("footer");
      }
      public function procesarActualizacion(){
        $id_cabina=$this->input->post("id_cabina");
        $datosCabinaEditado=array(
          "nombre_cab"=>$this->input->post("nombre_cab"),
          "marca_cab"=>$this->input->post("marca_cab"),
          "color_cab"=>$this->input->post("color_cab"),
          "tipo_cab"=>$this->input->post("tipo_cab"),

        );
        if ($this->cabina->actualizar($id_cabina,$datosCabinaEditado)) {
        //  echo "actualizacion EXITOSA";
        redirect("cabinas/index");
        }else{
          echo "error";
        }
      }
      public function guardarCabina(){
        $datosNuevosCabina=array(
          "nombre_cab"=>$this->input->post("nombre_cab"),
          "marca_cab"=>$this->input->post("marca_cab"),
          "color_cab"=>$this->input->post("color_cab"),
            "tipo_cab"=>$this->input->post("tipo_cab"),

        );
        if ($this->cabina->insertar($datosNuevosCabina)) {
          $this->session->set_flashdata("confirmacion","info cabina insertado exitosamente.");

        }else{
          $this->session->set_flashdata("error","Error al procesar.");
        }
        redirect("cabinas/index");
      }
      public function editar($id_cabina){
        $data["cabina"]=$this->cabina->consultarPorId($id_cabina);
        $this->load->view("header");
        $this->load->view("cabinas/editar",$data);
        $this->load->view("footer");
      }
      public function procesarEliminacion($id_cabina){
        if($this->cabina->eliminar($id_cabina)){
          redirect("cabinas/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }
 }
  }
?>
