<?php
  class Camiones extends CI_Controller{
      public function __construct(){
        parent::__construct();
        $this->load->model("camion");

      }
      public function index(){
        $data["listadoCamiones"]=$this->camion->consultarTodos();
        $this->load->view("header");
        $this->load->view("camiones/index",$data);
        $this->load->view("footer");
      }
      public function pesado(){
        $this->load->view("header");
        $this->load->view("Camiones/pesado");
        $this->load->view("footer");
      }
      public function procesarActualizacion(){
        $id_cam=$this->input->post("id_cam");
        $datosCamionEditado=array(
          "nombre_cam"=>$this->input->post("nombre_cam"),
          "marca_cam"=>$this->input->post("marca_cam"),
          "tonelaje_cam"=>$this->input->post("tonelaje_cam"),
          "color_cam"=>$this->input->post("color_cam"),
          "tipo_cam"=>$this->input->post("tipo_cam"),

        );
        if ($this->camion->actualizar($id_cam,$datosCamionEditado)) {
        //  echo "actualizacion EXITOSA";
        redirect("camiones/index");
        }else{
          echo "error";
        }
      }
      public function guardarCamion(){
        $datosNuevosCamion=array(
          "nombre_cam"=>$this->input->post("nombre_cam"),
          "marca_cam"=>$this->input->post("marca_cam"),
          "tonelaje_cam"=>$this->input->post("tonelaje_cam"),
          "color_cam"=>$this->input->post("color_cam"),
          "tipo_cam"=>$this->input->post("tipo_cam"),

        );
        if ($this->camion->insertar($datosNuevosCamion)) {
          $this->session->set_flashdata("confirmacion","info camion insertado exitosamente.");

        }else{
          $this->session->set_flashdata("error","Error al procesar.");
        }
        redirect("camiones/index");
      }
      public function editar($id_cam){
        $data["camion"]=$this->camion->consultarPorId($id_cam);
        $this->load->view("header");
        $this->load->view("camiones/editar",$data);
        $this->load->view("footer");
      }
      public function procesarEliminacion($id_cam){
        if($this->camion->eliminar($id_cam)){
          redirect("camione/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }
 }
  }
?>
