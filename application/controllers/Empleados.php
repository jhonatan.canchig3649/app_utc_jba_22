<?php

    class Empleados extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model('empleado');
        }

        public function index(){
          $data["listadoEmpelados"]=$this->empleado->consultarTodos();
            $this->load->view('header');
            $this->load->view('empleados/index',$data);
            $this->load->view('footer');
        }

        public function nuevo(){
            $this->load->view('header');
            $this->load->view('empleados/nuevo');
            $this->load->view('footer');
        }

        public function guardarEmpleado(){
          $datosNuevoEmpleado=array(
              "identificador_emp"=>$this->input->post("identificador_emp"),
              "nombre_emp"=>$this->input->post("nombre_emp"),
              "apellido_emp"=>$this->input->post("apellido_emp"),
              "direccion_emp"=>$this->input->post("direccion_emp"),
              "estado_emp"=>$this->input->post("estado_emp")
            );

            if ($this->empleado->insertar($datosNuevoEmpleado)) {
              $this->session->set_flashdata("confirmacion","Empleado insertado exitosamente.");
            } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
            }
            redirect("empleados/index");
        }

          function procesarEliminacion($id_emp){
                if ($this->empleado->eliminar($id_emp)) {
                  redirect("empleados/index");
                } else {
                  echo "ERROR AL ELIMINAR";
                }
        }

        function editar($id_emp){
          $data["empleado"] = $this->empleado->consultarId($id_emp);
                $this->load->view('header');
                $this->load->view('empleados/editar',$data);
                $this->load->view('footer');
        }

        public function procesarActualizacion(){
              $id_emp=$this->input->post('id_emp');
              $datosEmpleadoActualizado=array(
                "identificador_emp"=>$this->input->post("identificador_emp"),
                "nombre_emp"=>$this->input->post("nombre_emp"),
                "apellido_emp"=>$this->input->post("apellido_emp"),
                "direccion_emp"=>$this->input->post("direccion_emp"),
                "estado_emp"=>$this->input->post("estado_emp")
              );

              if ($this->empleado->actualizar($id_emp, $datosEmpleadoActualizado)) {
                $this->session->set_flashdata("confirmacion","Empleado Actualizado exitosamente.");

              }else{
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("empleados/index");
    }
}
 ?>
