<?php
  class Furgones extends CI_Controller{
      public function __construct(){
        parent::__construct();
        $this->load->model("furgon");

      }
      public function index(){
        $data["listadoFurgones"]=$this->furgon->consultarTodos();
        $this->load->view("header");
        $this->load->view("furgones/index",$data);
        $this->load->view("footer");
      }
      public function tipo(){
        $this->load->view("header");
        $this->load->view("furgones/tipo");
        $this->load->view("footer");
      }
      public function procesarActualizacion(){
        $id_fur=$this->input->post("id_fur");
        $datosFurgonEditado=array(
          "nombre_fur"=>$this->input->post("nombre_fur"),
          "marca_fur"=>$this->input->post("marca_fur"),
          "color_fur"=>$this->input->post("color_fur"),
          "tipo_fur"=>$this->input->post("tipo_fur"),

        );
        if ($this->furgon->actualizar($id_fur,$datosFurgonEditado)) {
        //  echo "actualizacion EXITOSA";
        redirect("furgones/index");
        }else{
          echo "error";
        }
      }
      public function guardarFurgon(){
        $datosNuevosFurgon=array(
          "nombre_fur"=>$this->input->post("nombre_fur"),
          "marca_fur"=>$this->input->post("marca_fur"),
          "color_fur"=>$this->input->post("color_fur"),
          "tipo_fur"=>$this->input->post("tipo_fur"),


        );
        if ($this->furgon->insertar($datosNuevosFurgon)) {
          $this->session->set_flashdata("confirmacion","info furgon insertado exitosamente.");

        }else{
          $this->session->set_flashdata("error","Error al procesar.");
        }
        redirect("furgones/index");
      }
      public function editar($id_fur){
        $data["furgon"]=$this->furgon->consultarPorId($id_fur);
        $this->load->view("header");
        $this->load->view("Furgones/editar",$data);
        $this->load->view("footer");
      }
      public function procesarEliminacion($id_fur){
        if($this->furgon->eliminar($id_fur)){
          redirect("furgones/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }
 }
  }
?>
