
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Cliente</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post" id="frm_nuevo_cliente">

<input class="form-control" type="hidden"  readonly name="id_cli" id="id_cli" value="<?php echo $cliente->id_cli; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_cli" id="identificador_cli" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $cliente->identificador_cli; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_cli" id="nombre_cli" placeholder="Por favor Ingrese el nombre"value="<?php echo $cliente->nombre_cli; ?>">
    <br>
    <br>
    <label  for="">APELLIDO</label>
    <input class="form-control" type="text" name="apellido_cli" id="apellido_cli" placeholder="Por favor Ingrese el apellido" value="<?php echo $cliente->apellido_cli; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="direccion_cli" id="direccion_cli" placeholder="Por favor Ingrese la dirección"value="<?php echo $cliente->direccion_cli; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_cli" id="estado_cli">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
      <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
      &nbsp;&nbsp;&nbsp
      <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
    </div>
</form>
</div>
<div class="col-md-2 "> </div>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        identificador_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_cli:{
          required:true,
          letras:true
        },
        apellido_cli:{
          required:true,
          letras:true
        },
        direccion_cli:{
          required:true,
          letras:true
        },
        estado_cli:{
          required:true
        }
      },

      messages:{
        identificador_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_cli:{
          required:"por favor ingrese el apellido",
          letras:"solo se acepta letras"
        },
        nombre_cli:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        direccion_cli:{
          required:"por favor ingrese una direccion",
          letras:"solo se acepta letras"
        },
        estado_cli:{
          required:"por favor seleccione un estado"
        }
      }
    });
</script>


<script type="text/javascript">
    $("#estado_cli").val("<?php echo $cliente->estado_cli; ?>");
</script>
