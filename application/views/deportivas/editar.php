<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url();?>/deportivas/procesarActualizacion"
					method="post" id="tbl">
					<input type="hidden" name="id_depo" id="id_depo" value="<?php echo $deportiva->id_depo; ?>">
					<br>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_depo" id="nombre_depo"
                class="form-control form-control-sm" placeholder="ingrese nombre" value="<?php echo $deportiva->nombre_depo; ?>" required >
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_depo" id="apellido_depo"
								class="form-control form-control-sm" placeholder="ingrese el apellido" value="<?php echo $deportiva->apellido_depo; ?>" required  >
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_depo" id="telefono_depo"
								class="form-control form-control-sm"placeholder="ingrese telefono" value="<?php echo $deportiva->telefono_depo; ?>" required pattern="[A-Za-z]+">
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="email" name="email_depo" id="email_depo"
                class="form-control form-control-sm" placeholder="ingrese email" value="<?php echo $deportiva->email_depo; ?>" >
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_depo" id="ciudad_depo"
                class="form-control form-control-sm" placeholder="ingrese la ciudad" value="<?php echo $deportiva->ciudad_depo; ?>" required  >
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_depo" id="agencia_depo"
                class="form-control form-control-sm" placeholder="ingrese la agencia" value="<?php echo $deportiva->agencia_depo; ?>" required  >
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <button class="btn btn-primary" type="submit" name="button"> ACTUALIZAR</button>

              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/deportivas/indexx" class="btn btn-warning">Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
