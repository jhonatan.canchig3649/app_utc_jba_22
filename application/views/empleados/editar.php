
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Empleado</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/empleados/procesarActualizacion" method="post" id="frm_nuevo_empleado">

<input class="form-control" type="hidden"  readonly name="id_emp" id="id_emp" value="<?php echo $empleado->id_emp; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_emp" id="identificador_emp" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $empleado->identificador_emp; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_emp" id="nombre_emp" placeholder="Por favor Ingrese el nombre"value="<?php echo $empleado->nombre_emp; ?>">
    <br>
    <br>
    <label  for="">APELLIDO</label>
    <input class="form-control" type="text" name="apellido_emp" id="apellido_emp" placeholder="Por favor Ingrese el apellido" value="<?php echo $empleado->apellido_emp; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="direccion_emp" id="direccion_emp" placeholder="Por favor Ingrese la dirección"value="<?php echo $empleado->direccion_emp; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_emp" id="estado_emp">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>
    <br>
    <br>
    <div align="center" >
      <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
      &nbsp;&nbsp;&nbsp
      <a href="<?php echo site_url(); ?>/empleados/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
    </div>
</form>
</div>
<div class="col-md-2 "> </div>
<script type="text/javascript">
    $("#frm_nuevo_empleado").validate({
      rules:{
        identificador_emp:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_emp:{
          required:true,
          letras:true
        },
        apellido_emp:{
          required:true,
          letras:true
        },
        direccion_emp:{
          required:true,
          letras:true
        },
        estado_emp:{
          required:true
        }
      },

      messages:{
        identificador_emp:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"Solo se acepta números"
        },
        apellido_emp:{
          required:"por favor ingrese el apellido",
          letras:"solo se acepta letras"
        },
        nombre_emp:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        direccion_emp:{
          required:"por favor ingrese una direccion",
          letras:"solo se acepta letras"
        },
        estado_emp:{
          required:"por favor seleccione un estado"
        }
      }
    });
</script>

<script type="text/javascript">
    $("#estado_emp").val("<?php echo $empleado->estado_emp; ?>");
</script>
