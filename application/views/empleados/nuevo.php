<br>
    <div class="col-md-2 "> </div>
    <div class="col-md-8" style="border: 2px solid darkgrey;">
      <form action="<?php echo site_url(); ?>/empleados/guardarEmpleado" method="post" id="frm_nuevo_empleado">
        <br>
      <center> <h3>Nuevo Empleado</h3> </center>
            <br>
            <label for="">Identificacion:</label><br>
            <input type="number" name="identificador_emp" id="identificador_emp" class="form-control" placeholder="Ingrese su número de Cedula">
            <br><br>
            <label for="">Nombre:</label><br>
            <input type="text" class="form-control" name="nombre_emp" id="nombre_emp" placeholder="Ingrese su Nombre">
            <br><br>
            <label for="">Apellidos:</label><br>
            <input type="text" class="form-control" name="apellido_emp" id="apellido_emp" placeholder="Ingrese sus Apellidos">
            <br><br>
            <label for="">Direccion:</label><br>
            <input type="text" class="form-control" name="direccion_emp" id="direccion_emp" placeholder="Ingrese la direccion">
            <br><br>
                <label for="">Estado</label>
                  <select class="form-control" name="estado_emp" id="estado_emp" >
                      <option value="">Seleccione...</option>
                      <option value="Activo">Activo</option>
                      <option value="Inactivo">Inactivo</option>
                  </select><br><br>
            <button type="submit" class="btn btn-info" name="button"><i class="fa-solid fa-floppy-disk"></i> Registrar</button>
            &nbsp;&nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/empleados/index" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
           <br><br>
      </form>
    </div>
    <div class="col-md-2"> </div>

    <script type="text/javascript">
        $("#frm_nuevo_empleado").validate({
          rules:{
            identificador_emp:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            nombre_emp:{
              required:true,
              letras:true
            },
            apellido_emp:{
              required:true,
              letras:true
            },
            direccion_emp:{
              required:true,
              letras:true
            },
            estado_emp:{
              required:true
            }
          },

          messages:{
            identificador_emp:{
              required:"Por favor ingrese el número de cédula",
              minlength:"La cédula debe tener mínimo 10 digitos",
              maxlength:"La cédula debe tener máximo 10 digitos",
              digits:"Solo se acepta números"
            },
            apellido_emp:{
              required:"por favor ingrese el apellido",
              letras:"solo se acepta letras"
            },
            nombre_emp:{
              required:"por favor ingrese el nombre",
              letras:"solo se acepta letras"
            },
            direccion_emp:{
              required:"por favor ingrese una direccion",
              letras:"solo se acepta letras"
            },
            estado_emp:{
              required:"por favor seleccione un estado"
            }
          }
        });
    </script>
