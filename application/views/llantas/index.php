<br>
<center>
  <h2>FORMULARIO LLANTAS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/llantas/rueda">Agregar Nombre de llantas</a>
</center>

<?php if ($listadoLlantas): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DELCAMION</th>
        <th class="text-center">MARCA DE LA LLANTA</th>
        <th class="text-center">TIPO CAMION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoLlantas->result() as $filaTemporal): ?>
        <tr>

          <td class="text-center">
            <?php echo $filaTemporal->id_llanta; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->camion_llan; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->marca_llan; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->tipo_llan; ?>
          </td>


          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
            <a class="btn btn-success"  href="<?php echo site_url(); ?>/llantas/editar/<?php echo $filaTemporal->id_llanta; ?>" ><i class="fa fa-pen"></i></a>

            <a class="btn btn-danger"  href="<?php echo site_url(); ?>/llantas/procesarEliminacion/<?php echo $filaTemporal->id_llanta; ?>" onclick="return confirm('¿Esta seguro?')"><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON LLANTAS REGISTRADAS</h1>
  </div>
<?php endif; ?>
<script type="text/javascript">
  function confirmarEliminacion(id_llanta){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/llantas/procesarEliminacion/"+ id_llan;

        }, true],
        ['<button>NO</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
//icoporar otoes de exploracio-->
  $("#tbl-llantas").DataTable();
</script>
