<div class="bg-white text-white" class="menu-tab-content ">

<div id="menu-tab-content-7"
                 >

<div class="w-full ff-univers-bold uppercase text-lg mt-1 mb-4" style="color: #333" class="form-control bg-white" >
                  <div class="bg-yellow">
                    Motos  Urbanas
                  </div> </div>


<center>
    <a class="product-item" href="https://www.yamahamotos.cl/producto/ybr-125/"
   target="_blank" class="zoom"
   data-menu-order="-11">
<div class="product-image">
<div class="yamaha-tags">
          </div>
          <div id="contenido">
   <img id="botella" src="https://www.yamahamotos.cl/wp-content/uploads/2016/09/YBR-125-roja-375x258.jpg" alt="botella con zoom" data-big="imagen_1280px.jpg" data-overlay="fondo.png" />
</div>

<div class="product-name " >
        NEW YBR-125
       </div>
<div class="product-price">
        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1.749.000</span>    </div>
<div class="product-price-meta">
            </div>
            <button type="button"   class="btn btn-secondary">
          <a href="<?php echo site_url();?>/motos/urbana"><h5 style="color: #ffff"> cotizar</h5></a>
          </button>
</a><br />
</center>
<center>
<a class="product-item" href="https://www.yamahamotos.cl/producto/xtz-125/"
   target="_blank"
   data-menu-order="-10">
<div class="product-image">
<div class="yamaha-tags">
                    </div>
        <img width="375" height="258" src="https://www.yamahamotos.cl/wp-content/uploads/2018/07/azul1-375x258.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" />    </div>
<div class="product-name">
        XTZ-125    </div>
<div class="product-price">
        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>2.299.000</span>    </div>
<div class="product-price-meta">
            </div>
            <button type="button" class="btn btn-secondary">
          <a href="<?php echo site_url();?>/motos/urbana"><h5 style="color: #ffff"> cotizar</h5></a>
          </button>
</a><br />
</center>
<center>
<a class="product-item" href="https://www.yamahamotos.cl/producto/xtz150/"
   target="_blank"
   data-menu-order="-8">
<div class="product-image">
<div class="yamaha-tags">
                    </div>
        <img width="375" height="258" src="https://www.yamahamotos.cl/wp-content/uploads/2019/06/XTZ-01-azul-375x258.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" />    </div>
<div class="product-name">
        XTZ-150    </div>
<div class="product-price">
        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>2.749.000</span>    </div>
<div class="product-price-meta">
            </div>
            <button type="button" class="btn btn-secondary">
          <a href="<?php echo site_url();?>/motos/urbana"><h5 style="color: #ffff"> cotizar</h5></a>
          </button>
</a><br />
</center>
<center>
<a class="product-item" href="https://www.yamahamotos.cl/producto/fzn150/"
   target="_blank"
   data-menu-order="-7">
<div class="product-image">
<div class="yamaha-tags">
                    </div>
        <img width="375" height="258" src="https://www.yamahamotos.cl/wp-content/uploads/2016/09/fzn150-yamaha-375x258.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" />    </div>
<div class="product-name">
        FZN150    </div>
<div class="product-price">
        <!--del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>2.190.000</span></del--><span class="price-from-label">Desde: </span><ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1.990.000</span></ins>    </div>
<div class="product-price-meta">
        Con financiamiento    </div>
        <button type="button" class="btn btn-secondary">
      <a href="<?php echo site_url();?>/motos/urbana"><h5 style="color: #ffff"> cotizar</h5></a>
      </button>
</a><br />
</center>
<center>
<a class="product-item" href="https://www.yamahamotos.cl/producto/fz-s/"
   target="_blank"
   data-menu-order="-6">
<div class="product-image">
<div class="yamaha-tags">
                    </div>
        <img width="375" height="258" src="https://www.yamahamotos.cl/wp-content/uploads/2021/01/FZ-S_rojo-e1617746652107-375x258.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" />    </div>
<div class="product-name">
        FZ-S    </div>
<div class="product-price">
        <span class="woocommerce-Price-amount amount">Próximamente</span>    </div>
<div class="product-price-meta">
            </div>
            <button type="button" class="btn btn-secondary">
          <a href="<?php echo site_url();?>/motos/urbana"><h5 style="color: #ffff"> cotizar</h5></a>
          </button>
</a><br />
</center>
  <center>
<a class="product-item" href="https://www.yamahamotos.cl/producto/new-fz-25/"
   target="_blank"
   data-menu-order="0">
<div class="product-image">
<div class="yamaha-tags">
                    </div>
        <img width="375" height="258" src="https://www.yamahamotos.cl/wp-content/uploads/2019/11/new_fz25_azul-e1574173827487-375x258.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" />    </div>
<div class="product-name">
        FZ-25    </div>
<div class="product-price">
        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>3.349.000</span>    </div>
<div class="product-price-meta">
            </div>
  <button type="button" class="btn btn-secondary">
<a href="<?php echo site_url();?>/motos/urbana"><h5 style="color: #ffff"> cotizar</h5></a>
</button>
</a>
</center>
</div>
<br> <br>
</div>
