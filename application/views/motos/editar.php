<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url();?>/motos/procesarActualizacion"
					method="post" id="tbl">
						<input type="hidden" name="id_moto" id="id_moto" value="<?php echo $moto->id_moto; ?>">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_mot" id="nombre_mot"
                class="form-control form-control-sm" placeholder="ingrese nombre" value="<?php echo $moto->nombre_mot; ?>" required />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">EMAIL</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="email_mot" id="email_mot"
								class="form-control form-control-sm" placeholder="ingrese el email" value="<?php echo $moto->email_mot; ?>" required  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">REGION</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="region_mot" id="region_mot"
								class="form-control form-control-sm"placeholder="ingrese telefono" value="<?php echo $moto->telefono_mot; ?>" required pattern="[A-Za-z]+"/>
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">COLOR</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="color_mot" id="color_mot"
                class="form-control form-control-sm" placeholder="ingrese el color" value="<?php echo $moto->color_mot; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">TELEFONO</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="telefono_mot" id="telefono_mot"
                class="form-control form-control-sm" placeholder="ingrese la agencia" value="<?php echo $moto->telefono_mot; ?>" required  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">

              <center>

                <button class="btn btn-primary" type="submit" name="button"> ACTUALIZAR</button>

              </center>
              <br>

          </div>
          <div class="col-md-6">

           <a href="<?php echo site_url(); ?>/motos/indexx" class="btn btn-warning">Cancelar</a>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
