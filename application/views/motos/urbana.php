<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  action="<?php echo Site_url();?>/motos/guardarMoto"
					method="post" id="validacion" >

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white"    type="text" name="nombre_mot" id="nombre_mot"
                placeholder="Ingrese nombre" required  />
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">EMAIL</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="email" name="email_mot" id="email_mot"
								class="form-control form-control-sm" placeholder="Ingrese  email"  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">REGION</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="region_mot" id="region_mot"
								class="form-control form-control-sm"placeholder="Ingrese region"  />
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">COLOR</label>
            <div class="col-sm-6">
              <input type="text" name="color_mot" id="color_mot"
              class="form-control bg-white" placeholder="Ingrese color"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">TELEFONO</label>
            <div class="col-sm-6">
              <input type="number" name="telefono_mot" id="telefono_mot"
                class="form-control bg-white" placeholder="Ingrese telefono"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>

          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <input type="submit" value="Guardar solicitud"
                  class="btn btn-primary" />
              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">

           <a href="<?php echo site_url(); ?>/motos/indexx" class="btn btn-warning"><i class="fa fa-circle-minus"></i>Cancelar</a>
        
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#validacion").validate({
		rules:{
			nombre_mot:{
				required:true},
			apellido_ad:{
				required:true,
				letras:true
			},
			region_mot:{
           required:true,

           digits:true},
				email_mot:{
					 required:true,
					 email:true

				 },
				 telefono_mot:{
 	           required:true,
 	          },

		},messages:{
			agencia_mot:{

				required:"por favor ingresa el numero entre 1-10",

				digits:"el tefelono solo acepta numeros"
			},
			telefono_mot:{
				required:"por favor ingresa el numero de telefono",
				minlength:"el numero de telefono debe tener minimo  10 digitos",
				maxlength:"el telefono debe tener maximo 10 digitos",
				digits:"el tefelono solo acepta numeros"
			},
			nombre_mot:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			apellido_mot:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			email_mot:{
				required:"por favor ingrese un correo electronico",
				email:"por favor ingrese correo valido"

			},

		}


	});
</script>
