<br>
<center>
<h3>Listado urbanas</h3>
<hr>
<a href="<?php echo site_url(); ?>/motos/index" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Nueva cotizacion</a>


<?php if ($listadoMotos): ?>
  <table class="table table-bordered table-striped table-hover" id="tbl">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">EMAIL</th>
          <th class="text-center">REGION</th>
          <th class="text-center">COLOR</th>
          <th class="text-center">TELEFONO</th>
          <th class="text-center">OPCIONES</th>

        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoMotos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_moto; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->email_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->region_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->color_mot; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->telefono_mot; ?>
              </td>

              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/motos/editar/<?php echo $filaTemporal->id_moto; ?>"><i class="fa fa-pen"></i></a>

                <a href="javascript:void(0)"
                  onclick="confirmarEliminacion('<?php echo$filaTemporal->id_moto; ?>');"
                  class="btn btn-danger">
                  <i class="fa fa-trash"> </i>

                </a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      </table>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>

  <?php endif; ?>
</center>
<script type="text/javascript">
function confirmarEliminacion(id_ad){

iziToast.question({
    timeout: 20000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'esta seguro de eliminar de forma permanente?',
    position: 'center',
    buttons: [
        ['<button><b>SI</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/motos/procesarEliminacion/"+id_ad;

        }, true],
        ['<button>NO</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

}


</script>

<script type="text/javascript">
$(document).ready( function () {
	$('#tbl').DataTable({
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
  });
} );
</script>
