
<br>

<div class="col-md-50" style="margin-left: 250px;">

    <div class="card">
        <div class="card-header">
            Registro de furgones
        </div>

        <div class="card-body">

        <form action="<?php echo site_url();?>/Furgones/procesarActualizacion" method="post" id="frm_editar_furgon" enctype="multipart/form-data">


            <div class = "form-group">
              <label for="">Nombre de la marca del camion:</label>
              <input type="text" class="form-control" value="" name="nombre_fur" id="nombre_fur" placeholder="Ingrese el nombre de la marca del vehiculo">
            </div>

            <div class = "form-group">
              <label for="">Marca del Furgon:</label>
              <input type="text" class="form-control" value="" name="marca_fur" id="marca_fur" placeholder="Ingrese la marca del furgon">
            </div>


          <div class="form-group">
          <label for="">Ingrese el color para el furgon :</label>
           <input type="text" class="form-control" value="" name="color_fur" id="color_fur" placeholder="Ingrese el color para la pintura">
            </div>


            <div class = "form-group">
              <label for="">Ingrese el tipo de furgon:</label>
              <input type="text" class="form-control" value="" name="tipo_fur" id="tipo_fur" placeholder="Ingrese el tipo de furgon segun su material">
            </div>


            </div>

            <center>
          
              <button type="submit" name="button" class="btn btn-primary">Agregar</button>

              &nbsp; &nbsp;
              <button type="submit" name="accion" value="Cancelar" href="<?php echo site_url(); ?>/furgones/index" class="btn btn-dark"><i class="fa fa-times"></i> Cancelar</button>
            </div>
            </center>


        </form>

        </div>



    </div>

</div>
<br>

<script type="text/javascript">
  $("#frm_editar_furgon").validate({
    rules:{
      nombre_fur:{
        required:true,
        letras:true

      },
      marca_fur:{
        required:true,
        letras:true
      },
      color_fur:{
        required:true,
        letras:true
      },
      tipo_fur:{
        required:true,
        letras:true
      },


    },
    messages:{
      nombre_cab:{
        required:"Ingrese el nombre del furgon a fabricar ",
        letras:"Solo se admiten letras"

      },
      marca_cab:{
        required:"Ingrese la marca del vehiculo para la fabricacion del furgon",
        letras:"Solo se admiten letras"
      },
      color_cab:{
        required:"Ingrese el color de pintura para el furgon",
        letras:"Solo se admiten letras"
      },
      tipo_cab:{
        required:"Ingrese el tipo de furgon sea este se alumio,refrigerado,tool",
        letras:"Solo se admiten letras"
      },


    }
  });
</script>
