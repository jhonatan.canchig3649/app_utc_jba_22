
<br>

<div class="col-md-6" style="margin-left: 250px;">

    <div class="card">
        <div class="card-header">
          Actualizacion Cabinas
        </div>
        <br>
        <br>

        <div class="card-body">

        <form action="<?php echo site_url();?>/cabinas/procesarActualizacion" method="post" id="frm_editar_cabina" enctype="multipart/form-data">
          <input type="hidden" name="id_cabina" id="id_cabina"value="<?php echo $cabina->id_cabina; ?>">


       <div class = "form-group">
         <label for="">Nombre de la cabina:</label>
         <input type="text" class="form-control" value="<?php echo $cabina->nombre_cab ?>" name="nombre_cab" id="nombre_cab" placeholder="Ingrese el nombre">
       </div>



            <div class = "form-group">
              <label for="">marca de la cabina:</label>
              <input type="text" class="form-control"  value="<?php echo $cabina->marca_cab ?>" name="marca_cab" id="marca_cab" placeholder="Ingrese la marca de la cabina">
            </div>


            <div class = "form-group">
              <label for="">color de cabina:</label>
              <input type="text" class="form-control"  value="<?php echo $cabina->color_cab ?>" name="color_cab" id="color_cab" placeholder="Ingrese el color de la cabina">
            </div>


            <div class = "form-group">
              <label for="">tipo de cabina:</label>
              <input type="text"class="form-control"  value="<?php echo $cabina->tipo_cab ?>" name="tipo_cab" id="tipo_cab" placeholder="Ingrese el tipo de la cabina en CHASIS/CABINADA">
            </div>


            <div class = "form-group">
                <label for="">FORMULARIO:</label>

            <select class="form-control" style="width:80%;" type="text" name="hemisferio_con" id="hemisferio_con" >
              <option value="">Seleccione una opcion</option>
              <option value="Procesadp"> Procesado</option>
              <option value="Rechazado">Rechazado</option>

            </select>
            </div>




            <center>
            <div class="btn-group" role="group" aria-label="">
              <button type="submit" name="button" class="btn btn-primary">Actualizar</button>
              <button type="submit" name="accion" value="Cambiar" class="btn btn-primary">Cambiar</button>
              &nbsp; &nbsp;
              <button type="submit" name="accion" value="Cancelar" href="<?php echo site_url();  ?>/welcome/index" class="btn btn-dark"><i class="fa fa-times-circle"></i> Cancelar</button>
            </div>
            </center>


        </form>

        </div>



    </div>

</div>
<br>
<script style="" type="text/javascript">
  //Activando el pais seleccionado para el cliente
  $("#hemisferio_con").val("<?php echo $continente->hemisferio_con; ?>");

</script>

<script type="text/javascript">
  $("#frm_editar_cabina").validate({
    rules:{
      nombre_cab:{
        required:true,
        letras:true

      },
      marca_cab:{
        required:true,
        letras:true
      },
      color_cab:{
        required:true,
        letras:true
      },
      tipo_cab:{
        required:true,
        letras:true
      },
      hemisferio_con:{
        required:true,

      },

    },
    messages:{
      nombre_cab:{
        required:"Ingrese el nombre",
        letras:"Solo se admiten letras"

      },
      marca_cab:{
        required:"Ingrese la marca del vehiculo para definir la cabina",
        letras:"Solo se admiten letras"
      },
      color_cab:{
        required:"Ingrese el color de la cabina",
        letras:"Solo se admiten letras"
      },
      tipo_cab:{
        required:"Ingrese el tipo se chasis o cabinado",
        letras:"Solo se admiten letras"
      },

      hemisferio_con:{
        required:"Seleccione una opcion",

      }
    }
  });
</script>
