
<br>

<div class="col-md-30" style="margin-left: 50%;">

    <div class="card">
        <div class="card-header">
            Registro de cabinas
        </div>

        <div class="card-body">

        <form action="<?php echo site_url();?>/Cabinas/guardarCabina" method="post" id="frm_metal_cabina" enctype="multipart/form-data">


            <div class = "form-group">
              <label for="">Nombre:</label>
              <input type="text" class="form-control" value="" name="nombre_cab" id="nombre_cab" placeholder="Ingrese el nombre">
            </div>

            <div class = "form-group">
              <label for="">Ingrese la Marca de la cabina:</label>
              <input type="text" class="form-control" value="" name="marca_cab" id="marca_cab" placeholder="Ingrese la marca de la cabina">
            </div>


          <div class="form-group">
          <label for="">Ingrese el color :</label>
           <input type="text" class="form-control" value="" name="color_cab" id="color_cab" placeholder="Ingrese el color de la cabina">
            </div>


            <div class = "form-group">
              <label for="">Ingrese el tipo de la cabina:</label>
              <input type="text" class="form-control" value="" name="tipo_cab" id="tipo_cab" placeholder="Ingrese el tipo de la cabina">
            </div>



            <div class = "form-group">
            <label for="text">formulario atendido:</label>

            <select class="form-control" style="width:80%;" type="text" name="hemisferio_con" id="hemisferio_con" >
              <option value="">Seleccione una opcion</option>
              <option value="si"> si </option>
              <option value="no">no</option>

            </select>
            </div>

            <center>
            <div class="btn-group" role="group" aria-label="">
              <button type="submit" name="button" class="btn btn-primary">Agregar</button>

              &nbsp; &nbsp;
              <button type="submit" name="accion" value="Cancelar" href="<?php echo site_url(); ?>/cabinas/index" class="btn btn-dark"><i class="fa fa-times"></i> Cancelar</button>
            </div>
            </center>


        </form>

        </div>



    </div>

</div>
<br>
<script type="text/javascript">
  $("#frm_metal_cabina").validate({
    rules:{
      nombre_cab:{
        required:true,
        letras:true

      },
      marca_cab:{
        required:true,
        letras:true
      },
      color_cab:{
        required:true,
        letras:true
      },
      tipo_cab:{
        required:true,
        letras:true
      },
      hemisferio_con:{
        required:true,

      },

    },
    messages:{
      nombre_cab:{
        required:"Ingrese el nombre",
        letras:"Solo se admiten letras"

      },
      marca_cab:{
        required:"Ingrese la marca del vehiculo para definir la cabina",
        letras:"Solo se admiten letras"
      },
      color_cab:{
        required:"Ingrese el color de la cabina",
        letras:"Solo se admiten letras"
      },
      tipo_cab:{
        required:"Ingrese el tipo se chasis o cabinado",
        letras:"Solo se admiten letras"
      },

      hemisferio_con:{
        required:"Seleccione una opcion",

      }
    }
  });
</script>
