<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Actualizar Vehiculo</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/vehiculos/procesarActualizacion" method="post" id="frm_nuevo_vehiculo">

<input class="form-control" type="hidden"  readonly name="id_veh" id="id_veh" value="<?php echo $vehiculo->id_veh; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_veh" id="identificador_veh" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $vehiculo->identificador_veh; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_veh" id="nombre_veh" placeholder="Por favor Ingrese el nombre"value="<?php echo $vehiculo->nombre_veh; ?>">
    <br>
    <br>
    <label for="">Color</label>
      <select class="form-control" name="color_veh" id="color_veh" >
          <option value="">Seleccione...</option>
          <option value="Azul">Azul</option>
          <option value="Negro">Negro</option>
          <option value="Blanco">Blanco</option>
          <option value="Rojo">Rojo</option>
      </select><br><br>
      <label for="">Tipo</label>
        <select class="form-control" name="tipo_veh" id="tipo_veh" >
            <option value="">Seleccione...</option>
            <option value="Camioneta">Camioneta</option>
            <option value="Automovil">Automovil</option>
            <option value="SUV">SUV</option>
        </select><br><br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_veh" id="estado_veh">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
      <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
      &nbsp;&nbsp;&nbsp
      <a href="<?php echo site_url(); ?>/vehiculos/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
    </div>
</form>
</div>
<div class="col-md-2 "> </div>

<script type="text/javascript">
    $("#frm_nuevo_vehiculo").validate({
      rules:{
        identificador_veh:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_veh:{
          required:true,
          letras:true
        },
        color_veh:{
          required:true
        },
        tipo_veh:{
          required:true
        },
        estado_veh:{
          required:true
        }
      },

      messages:{
        identificador_veh:{
          required:"Por favor ingrese el número de Identificacion",
          minlength:"Debe tener mínimo 10 digitos",
          maxlength:"Debe tener máximo 10 digitos",
          digits:"Solo acepta números"
        },
        nombre_veh:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        color_veh:{
          required:"por favor seleccione uno"
        },
        tipo_veh:{
          required:"por favor seleccione uno"
        },
        estado_veh:{
          required:"por favor seleccione uno"
        }
      }
    });
</script>

<script type="text/javascript">
    $("#color_veh").val("<?php echo $vehiculo->color_veh; ?>");
    $("#tipo_veh").val("<?php echo $vehiculo->tipo_veh; ?>");
    $("#estado_veh").val("<?php echo $vehiculo->estado_veh; ?>");
</script>
