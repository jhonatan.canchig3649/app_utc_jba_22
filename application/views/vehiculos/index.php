<div class="col-md-1"></div>
<div class="col-md-10">
<br>
<center>
<h3>Listado de Vehiculos</h3>
<hr>
<a href="<?php echo site_url(); ?>/vehiculos/nuevo" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Nuevo </a>
</center>
<?php if ($listadoVehiculos): ?>
  <br>
  <table class="table table-bordered table-striped table-hover" id="tbl-vehiculos">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">IDENTIFICACION</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">COLOR</th>
          <th class="text-center">TIPO</th>
          <th class="text-center">ESTADO</th>
          <th class="text-center">OPCIONES</th>
        </tr>
      </thead>

      <tbody>
          <?php foreach ($listadoVehiculos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->identificador_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->color_veh; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->tipo_veh; ?>
              </td>
              <td class="text-center">
              <?php if ($filaTemporal->estado_veh=="Activo"): ?>
                  <div class="alert alert-success">Activo</div>
              <?php else: ?>
                  <div class="alert alert-danger">Inactivo</div>
              <?php endif; ?>
              </td>
              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/vehiculos/editar/<?php echo $filaTemporal->id_veh; ?>"><strong style="color:white;"> <i class="fa fa-pen"></i></strong></a>
                <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_veh; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      </table>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>
  <?php endif; ?>
</div>
<div class="col-md-1"></div>


<script type="text/javascript">
    function confirmarEliminacion(id_veh){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 1050,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el vehiculo de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/vehiculos/procesarEliminacion/"+id_veh;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
$(document).ready( function () {
	$('#tbl-vehiculos').DataTable({
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
  });
} );
</script>
