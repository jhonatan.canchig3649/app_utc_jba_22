
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Informacion de Repuesto</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/repuestos/procesarActualizacion" method="post" id="frm_nuevo_repuesto">

    <input class="form-control" type="hidden"  readonly name="id_rep" id="id_rep" value="<?php echo $repuesto->id_rep; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_rep" id="identificador_rep" placeholder="Por favor Ingrese el numero de serie" value="<?php echo $repuesto->identificador_rep; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_rep" id="nombre_rep" placeholder="Por favor Ingrese el nombre"value="<?php echo $repuesto->nombre_rep; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="descripcion_rep" id="descripcion_rep" placeholder="Por favor Ingrese la descripcion"value="<?php echo $repuesto->descripcion_rep; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_rep" id="estado_rep">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
      <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
      &nbsp;&nbsp;&nbsp
      <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
    </div>
</form>
</div>
<div class="col-md-2 "> </div>

<script type="text/javascript">
    $("#frm_nuevo_repuesto").validate({
      rules:{
        identificador_rep:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_rep:{
          required:true,
          letras:true
        },

        descripcion_rep:{
          required:true,
          letras:true
        },
        estado_rep:{
          required:true
        }
      },

      messages:{
        identificador_rep:{
          required:"Por favor ingrese el número de Identificacion",
          minlength:"Debe tener mínimo 10 digitos",
          maxlength:"Debe tener máximo 10 digitos",
          digits:"Solo acepta números"
        },
        nombre_rep:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        descripcion_rep:{
          required:"por favor ingrese una descripcion",
          letras:"solo se acepta letras"
        },
        estado_rep:{
          required:"por favor seleccione un estado"
        }
      }
    });
</script>

<script type="text/javascript">
    $("#estado_rep").val("<?php echo $repuesto->estado_rep; ?>");

</script>
