<br>
    <div class="col-md-2 "> </div>
    <div class="col-md-8" style="border: 2px solid darkgrey;">
      <form action="<?php echo site_url(); ?>/repuestos/guardarRepuestos" method="post" id="frm_nuevo_repuesto">
        <br>
      <center> <h3>FORMULARIO DE REGISTRO</h3> </center>
            <br>

            <label for="">Identificacion:</label><br>
            <input type="number" name="identificador_rep" id="identificador_rep" class="form-control" placeholder="Ingrese su número de serie">
            <br><br>
            <label for="">Nombre:</label><br>
            <input type="text" class="form-control" name="nombre_rep" id="nombre_rep" placeholder="Ingrese su Nombre">
            <br><br>
            <label for="">Descripcion:</label><br>
            <input type="text" class="form-control" name="descripcion_rep" id="descripcion_rep" placeholder="Ingrese la descripcion">
            <br><br>
                <label for="">Estado</label>
                  <select class="form-control" name="estado_rep" id="estado_rep" >
                      <option value="">Seleccione...</option>
                      <option value="Activo">Activo</option>
                      <option value="Inactivo">Inactivo</option>
                  </select><br><br>
            <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Registrar</button>
            &nbsp;&nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/repuestos/index" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
           <br><br>
      </form>
    </div>
    <div class="col-md-2"> </div>
    <script type="text/javascript">
        $("#frm_nuevo_repuesto").validate({
          rules:{
            identificador_rep:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            nombre_rep:{
              required:true,
              letras:true
            },

            descripcion_rep:{
              required:true,
              letras:true
            },
            estado_rep:{
              required:true
            }
          },

          messages:{
            identificador_rep:{
              required:"Por favor ingrese el número de Identificacion",
              minlength:"Debe tener mínimo 10 digitos",
              maxlength:"Debe tener máximo 10 digitos",
              digits:"Solo acepta números"
            },
            nombre_rep:{
              required:"por favor ingrese el nombre",
              letras:"solo se acepta letras"
            },
            descripcion_rep:{
              required:"por favor ingrese una descripcion",
              letras:"solo se acepta letras"
            },
            estado_rep:{
              required:"por favor seleccione un estado"
            }
          }
        });
    </script>
