<div class="col-md-1"></div>
<div class="col-md-10">
  <center>
<h3>Lista de Repuestos</h3>
<hr>
<a href="<?php echo site_url(); ?>/repuestos/nuevo" class="btn btn-info"> <i class="fa fa-plus"></i> Agregar Nuevo </a>
<br>
</center>
<?php if ($listadoRepuestos): ?>
  <br>
  <table class="table table-bordered table-striped table-hover" id="tbl-repuestos">
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">IDENTIFICACION</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">DESCRIPCION</th>
          <th class="text-center">ESTADO</th>
          <th class="text-center">OPCIONES</th>
        </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoRepuestos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                  <?php echo $filaTemporal->id_rep; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->identificador_rep; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->nombre_rep; ?>
              </td>
              <td class="text-center">
                  <?php echo $filaTemporal->descripcion_rep; ?>
              </td>
              <td class="text-center">
              <?php if ($filaTemporal->estado_rep=="Activo"): ?>
                  <div class="alert alert-success">Activo</div>
              <?php else: ?>
                  <div class="alert alert-danger">Inactivo</div>
              <?php endif; ?>
              </td>
              <td class="text-center">
                <a class="btn btn-warning" href="<?php echo site_url();
                ?>/repuestos/editar/<?php echo $filaTemporal->id_rep; ?>"><strong style="color:white;"> <i class="fa fa-pen"></i> </strong></a>
                <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_rep; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
              </td>
            </tr>
          <?php endforeach; ?>
      </tbody>
      </table>
      <?php else: ?>
        <div class="alert alert-danger">
            <h3>No se encontraron registros</h3>
        </div>

  <?php endif; ?>

</div>
<div class="col-md-1"></div>

<script type="text/javascript">
    function confirmarEliminacion(id_emp){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 1050,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el repuesto de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/repuestos/procesarEliminacion/"+id_emp;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
$(document).ready( function () {
	$('#tbl-repuestos').DataTable({
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
  });
} );
</script>
