<br>
<center>
  <h2>Listado de Camiones</h2>
</center>

<hr>

<br>
<center>
  <a href="<?php echo site_url();?>/camiones/pesado"><i class="fa fa-plus-circle fa-lg"></i> Nuevo </a>
</center>
<br>
<br>

<?php if ($listadoCamiones): ?>
  <table class="table" id="tbl-camiones">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE:</th>
        <th class="text-center">MARCA</th>
        <th class="text-center">TONELAJE</th>
        <th class="text-center">COLOR</th>
          <th class="text-center">TIPO</th>
        <th class="text-center">OPCION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoCamiones->result() as $filaTemporal): ?>
        <tr>
          <td class="text-center">
          <?php echo $filaTemporal->id_cam;?></td>
          <td class="text-center">
          <?php echo $filaTemporal->nombre_cam;?></td>
          <td class="text-center">
          <?php echo $filaTemporal->marca_cam;?></td>
          <td class="text-center">
          <?php echo $filaTemporal->tonelaje_cam;?></td>
          <td class="text-center">
          <?php echo $filaTemporal->color_cam;?></td>
          <td class="text-center">
          <?php echo $filaTemporal->tipo_cam;?></td>

          <td class="text-center">
            <a href="<?php echo site_url()?>/Camiones/editar/<?php echo $filaTemporal->id_cam;?>" class="btn btn-primary"> <i class="fa fa-pen"></i> </a>
            <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cam; ?>');" class="btn btn-danger"> <i class="fa fa-trash"></i>
                          </a>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>No se ha encontrado Cabinas registradas</h1>

  </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_cam){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar ?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/camiones/procesarEliminacion/"+id_cam;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

            <script type="text/javascript">
                $(document).ready( function () {
                  $('#tbl-camiones').DataTable({
                      dom: 'Blfrtip',
                      buttons: [
                          'copyHtml5',
                          'excelHtml5',
                          'csvHtml5',
                          'pdfHtml5'
                        ],
                });
              });
            </script>
