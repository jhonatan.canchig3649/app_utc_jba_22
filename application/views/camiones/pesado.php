
<br>

<div class="col-md-6" style="margin-left: 250px;">

    <div class="card">
        <div class="card-header">
          Registro de datos Camiones
        </div>

        <div class="card-body">

        <form action="<?php echo site_url();?>/Camiones/guardarCamion" method="post" id="frm_pesado_camion" enctype="multipart/form-data">
          <input type="hidden" name="id_cam" id="id_cam"value="<?php echo $camion->id_cam; ?>">


       <div class = "form-group">
         <label for="">Nombre de camion:</label>
         <input type="text" class="form-control" value="<?php echo $camion->nombre_cam ?>" name="nombre_cam" id="nombre_cam" placeholder="Ingrese el nombre del camion">
       </div>



            <div class = "form-group">
              <label for="">marca del camion:</label>
              <input type="text" class="form-control"  value="<?php echo $camion->marca_cam ?>" name="marca_cam" id="marca_cam" placeholder="Ingrese la marca del camion">
            </div>


            <div class = "form-group">
              <label for="">Ingrese tonelaje del camion:</label>
              <input type="text" class="form-control"  value="<?php echo $camion->tonelaje_cam ?>" name="tonelaje_cam" id="tonelaje_cam" placeholder="Ingrese el tonelaje del camion">
            </div>

            <div class = "form-group">
              <label for="">Ingrese el color :</label>
              <input type="text" class="form-control"  value="<?php echo $camion->color_cam ?>" name="color_cam" id="color_cam" placeholder="Ingrese el color">
            </div>

            <div class = "form-group">
              <label for="">tipo de camion en chasis / cabinado:</label>
              <input type="text"class="form-control"  value="<?php echo $camion->tipo_cam ?>" name="tipo_cam" id="tipo_cam" placeholder="Ingrese el tipo de la camion en CHASIS/CABINADA">
            </div>


            <div class = "form-group">
                <label for="">FORMULARIO:</label>

            <select class="form-control" style="width:80%;" type="text" name="hemisferio_con" id="hemisferio_con" >
              <option value="">Seleccione una opcion</option>
              <option value="Procesadp"> Procesado</option>
              <option value="Rechazado">Rechazado</option>

            </select>
            </div>




            <center>
            <div class="btn-group" role="group" aria-label="">
              <button type="submit" name="button" class="btn btn-primary">Actualizar</button>
              <button type="submit" name="accion" value="Cambiar" class="btn btn-primary">Cambiar</button>
              &nbsp; &nbsp;
              <button type="submit" name="accion" value="Cancelar" href="<?php echo site_url();  ?>/welcome/index" class="btn btn-dark"><i class="fa fa-times-circle"></i> Cancelar</button>
            </div>
            </center>


        </form>

        </div>



    </div>

</div>
<br>
<script style="" type="text/javascript">
  //Activando el pais seleccionado para el cliente
  $("#hemisferio_con").val("<?php echo $continente->hemisferio_con; ?>");

</script>

<script type="text/javascript">
  $("#frm_pesado_camion").validate({
    rules:{
      nombre_cam:{
        required:true,
        letras:true

      },
      marca_cam:{
        required:true,
        letras:true
      },
      tonelaje_cam:{
        required:true,
        digits:true
      },
      color_cam:{
        required:true,
        letras:true
      },
      tipo_cam:{
        required:true,
        letras:true
      },
      hemisferio_con:{
        required:true,

      },

    },
    messages:{
      nombre_cam:{
        required:"Ingrese el nombre",
        letras:"Solo se admiten letras"

      },
      marca_cab:{
        required:"Ingrese la marca del camion",
        letras:"Solo se admiten letras"
      },
      tonelaje_cam:{
        required:"Ingrese el tonelaje del camion",
        letras:"Solo se admiten letras"
      },
      color_cam:{
        required:"Ingrese el color del camion",
        letras:"Solo se admiten numeros"
      },
      tipo_cam:{
        required:"Ingrese el tipo se chasis o cabinado",
        letras:"Solo se admiten letras"
      },

      hemisferio_con:{
        required:"Seleccione una opcion",

      }
    }
  });
</script>
