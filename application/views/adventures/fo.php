<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  class="form-control input-sm required"action="<?php echo Site_url(); ?>/adventures/guardarAdventure"
					method="post" id="validacion"	>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_ad" id="nombre_ad"
                class="form-control form-control-sm" placeholder="ingrese nombre"   />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_ad" id="apellido_ad"
								class="form-control form-control-sm" placeholder="ingrese el apellido"  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_ad" id="telefono_ad"
								class="form-control form-control-sm"placeholder="ingrese telefono" />
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="email" name="email_ad" id="email_ad"
                class="form-control form-control-sm" placeholder="ingrese email"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_ad" id="ciudad_ad"
                class="form-control form-control-sm" placeholder="ingrese la ciudad"   />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_ad" id="agencia_ad"
                class="form-control form-control-sm" placeholder="ingrese la agencia"   />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
						<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
              <center>
                <input type="submit" value="Guardar solicitud"
                  class="btn btn-primary" />
              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
						<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

           <a href="<?php echo site_url(); ?>/adventures/indexx" class="btn btn-warning"><i class="fa fa-circle-minus"></i>Cancelar</a>
         
          </div>
        </div>


				</form>


			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#validacion").validate({
		rules:{
			nombre_ad:{
				required:true},
			apellido_ad:{
				required:true,
				letras:true
			},
			telefono_ad:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true},
				email_ad:{
					 required:true,
					 email:true

				 },
				 ciudad_ad:{
					 required:true,
					 letras:true

				 },
				 agencia_ad:{
					 required:true,
					 letras:true

				 }

		},messages:{
			telefono_ad:{
				required:"por favor ingresa el numero de telefono",
				minlength:"el numero de telefono debe tener minimo  10 digitos",
				maxlength:"el telefono debe tener maximo 10 digitos",
				digits:"el tefelono solo acepta numeros"
			},
			nombre_ad:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			apellido_ad:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			email_ad:{
				required:"por favor ingrese un correo electronico",
				email:"por favor ingrese correo valido"

			},
			ciudad_ad:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese la ciudad"

			},
		agencia_ad:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese la agencia"

			}
		}


	});
</script>
