<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  action="<?php echo Site_url();?>/nakes/guardarNake"
					method="post" id="validacion" >
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white" type="text" name="nombre_na" id="nombre_na"
                class="form-control form-control-sm" placeholder="ingrese nombre"  />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_na" id="eapellido_na"
								class="form-control form-control-sm" placeholder="ingrese el apellido"  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white"  type="number" name="telefono_na" id="telefono_na"
								 placeholder="Ingrese telefono"  />
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="email" name="email_na" id="email_na"
                class="form-control form-control-sm" placeholder="ingrese email"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_na" id="ciudad_na"
                class="form-control form-control-sm" placeholder="ingrese la ciudad"   />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_na" id="agencia_na"
                class="form-control form-control-sm" placeholder="ingresela agencia"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <input type="submit" value="Guardar solicitud"
                  class="btn btn-primary" />
              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/nakes/indexx" class="btn btn-warning"><i class="fa fa-circle-minus"></i>Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#validacion").validate({
		rules:{
			nombre_na:{
				required:true},
			apellido_na:{
				required:true,
				letras:true
			},
			telefono_na:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true},
				email_na:{
					 required:true,
					 email:true

				 },
				 ciudad_na:{
					 required:true,
					 letras:true

				 },
				 agencia_na:{
					 required:true,
					 letras:true

				 }

		},messages:{
			telefono_na:{
				required:"por favor ingresa el numero de telefono",
				minlength:"el numero de telefono debe tener minimo  10 digitos",
				maxlength:"el telefono debe tener maximo na digitos",
				digits:"el tefelono solo acepta numeros"
			},
			nombre_na:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			apellido_na:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			email_na:{
				required:"por favor ingrese un correo electronico",
				email:"por favor ingrese correo valido"

			},
			ciudad_na:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese la ciudad"

			},
		agencia_na:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese la agencia"

			}
		}


	});
</script>
