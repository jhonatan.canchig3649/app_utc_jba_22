<div class="col-md-12">
<div  class="bg-white text-black">
		<div class="card bg-white">
			<div class="card-header" > Registro para cotizar </div>
			<div class="card-body">
				<form  action="<?php echo Site_url();?>/sports/guardarSport"
					method="post" id="validacion">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">NOMBRE</label>
            <div class="col-sm-6" >
              <input class="form-control bg-white"type="text" name="nombre_sp" id="nombre_sp"
                class="form-control form-control-sm" placeholder="ingrese nombre"  />
              <small class="form-text text-white"
              ></small>
            </div>
          </div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">APELLIDO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="text" name="apellido_sp" id="eapellido_sp"
								class="form-control form-control-sm" placeholder="ingrese el apellido"  />
							<small class="form-text text-danger"
							></small>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label">TELEFONO</label>
						<div class="col-sm-6">
							<input class="form-control bg-white" type="number" name="telefono_sp" id="telefono_sp"
								class="form-control form-control-sm"placeholder="ingrese telefono" required pattern="[A-Za-z]+" />
							<small class="form-text text-danger"
							 ></small>
						</div>
					</div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">EMAIL</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="email" name="email_sp" id="email_sp"
                class="form-control form-control-sm" placeholder="ingrese email"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CIUDAD</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="ciudad_sp" id="ciudad_sp"
                class="form-control form-control-sm" placeholder="ingrese la ciudad"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">AGENCIA</label>
            <div class="col-sm-6">
              <input class="form-control bg-white" type="text" name="agencia_sp" id="agencia_sp"
                class="form-control form-control-sm" placeholder="ingresela agencia"  />
              <small class="form-text text-danger"
               ></small>
            </div>
          </div>
          </div>
				<div class="row">
          <div class="col-md-6">
            <div class="col-md-12">
              <center>
                <input type="submit" value="Guardar solicitud"
                  class="btn btn-primary" />
              </center>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <center>
           <a href="<?php echo site_url(); ?>/sports/indexx" class="btn btn-warning"><i class="fa fa-circle-minus"></i>Cancelar</a>
         </center>
          </div>
        </div>


				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#validacion").validate({
		rules:{
			nombre_sp:{
				required:true},
			apellido_sp:{
				required:true,
				letras:true
			},
			telefono_sp:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true},
				email_sp:{
					 required:true,
					 email:true

				 },
				 ciudad_sp:{
					 required:true,
					 letras:true

				 },
				 agencia_sp:{
					 required:true,
					 letras:true

				 }

		},messages:{
			telefono_sp:{
				required:"por favor ingresa el numero de telefono",
				minlength:"el numero de telefono debe tener minimo  10 digitos",
				maxlength:"el telefono debe tener maximo 10 digitos",
				digits:"el tefelono solo acepta numeros"
			},
			nombre_sp:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			apellido_sp:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese el nombre"

			},
			email_sp:{
				required:"por favor ingrese un correo electronico",
				email:"por favor ingrese correo valido"

			},
			ciudad_sp:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese la ciudad"

			},
		agencia_sp:{
				required:"por favor ingrese letras",
				letras:"por favor ingrese la agencia"

			}
		}


	});
</script>
