
<div class="col-md-2 "> </div>
<div class="col-md-8">
  <center>
    <h1> Editar Sucursal</h1>
    <br>
    <hr>
    <br>
</center>
<form action="<?php echo site_url(); ?>/sucursales/procesarActualizacion" method="post" id="frm_nueva_sucursal">

<input class="form-control" type="hidden"  readonly name="id_suc" id="id_suc" value="<?php echo $sucursal->id_suc; ?>">
    <br>
    <br>
    <label  for="">IDENTIFICACIÓN</label>
    <input class="form-control"type="number" name="identificador_suc" id="identificador_suc" placeholder="Por favor Ingrese la Identificacion" value="<?php echo $sucursal->identificador_suc; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" type="text" name="nombre_suc" id="nombre_suc" placeholder="Por favor Ingrese el nombre"value="<?php echo $sucursal->nombre_suc; ?>">
    <br>
    <br>
    <label  for="">Encargado</label>
    <input class="form-control" type="text" name="encargado_suc" id="encargado_suc" placeholder="Por favor Ingrese el nombre del encargado" value="<?php echo $sucursal->encargado_suc; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" type="text" name="direccion_suc" id="direccion_suc" placeholder="Por favor Ingrese la dirección"value="<?php echo $sucursal->direccion_suc; ?>">
    <br>
    <br>
    <label for="">ESTADO</label>

    <select class="form-control" name="estado_suc" id="estado_suc">
    <option value="">Selecione...</option>
        <option value="Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>

    <br>
    <br>
    <div align="center" >
      <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
      &nbsp;&nbsp;&nbsp
      <a href="<?php echo site_url(); ?>/sucursales/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
    </div>
</form>
</div>
<div class="col-md-2 "> </div>

<script type="text/javascript">
    $("#frm_nueva_sucursal").validate({
      rules:{
        identificador_suc:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_suc:{
          required:true,
          letras:true
        },
        encargado_suc:{
          required:true,
          letras:true
        },
        direccion_suc:{
          required:true,
          letras:true
        },
        estado_suc:{
          required:true
        }
     },

      messages:{
        identificador_suc:{
          required:"Por favor ingrese el número de identificacion",
          minlength:"Debe tener mínimo 10 digitos",
          maxlength:"Debe tener máximo 10 digitos",
          digits:"Solo acepta números"
        },
        nombre_suc:{
          required:"por favor ingrese el apellido",
          letras:"solo se acepta letras"
        },
        encargado_suc:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        direccion_suc:{
          required:"por favor ingrese una direccion",
          letras:"solo se acepta letras"
        },
        estado_suc:{
          required:"por favor seleccione un estado"
        }
      }
    });
</script>

<script type="text/javascript">
    $("#estado_suc").val("<?php echo $sucursal->estado_suc; ?>");
</script>
