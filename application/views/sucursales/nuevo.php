<br>
    <div class="col-md-2 "> </div>
    <div class="col-md-8" style="border: 2px solid darkgrey;">
      <form action="<?php echo site_url(); ?>/sucursales/guardarSucursal" method="post" id="frm_nueva_sucursal">
        <br>
      <center> <h3>Nueva Sucursal</h3> </center>
            <br>
            <label for="">Identificacion:</label><br>
            <input type="number" name="identificador_suc" id="identificador_suc" class="form-control" placeholder="Ingrese su número de local">
            <br><br>
            <label for="">Nombre:</label><br>
            <input type="text" class="form-control" name="nombre_suc" id="nombre_suc" placeholder="Ingrese el nombre de la sucursal">
            <br><br>
            <label for="">Encargado:</label><br>
            <input type="text" class="form-control" name="encargado_suc" id="encargado_suc" placeholder="Ingrese el nombre del encargado">
            <br><br>
            <label for="">Direccion:</label><br>
            <input type="text" class="form-control" name="direccion_suc" id="direccion_suc" placeholder="Ingrese la direccion">
            <br><br>
                <label for="">Estado</label>
                  <select class="form-control" name="estado_suc" id="estado_suc" >
                      <option value="">Seleccione...</option>
                      <option value="Activo">Activo</option>
                      <option value="Inactivo">Inactivo</option>
                  </select><br><br>
                  <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Registrar</button>
                  &nbsp;&nbsp;&nbsp
                  <a href="<?php echo site_url(); ?>/sucursasuc/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
           <br><br>
      </form>
    </div>
    <div class="col-md-2"> </div>

<script type="text/javascript">
    $("#frm_nueva_sucursal").validate({
      rules:{
        identificador_suc:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_suc:{
          required:true,
          letras:true
        },
        encargado_suc:{
          required:true,
          letras:true
        },
        direccion_suc:{
          required:true,
          letras:true
        },
        estado_suc:{
          required:true
        }
     },

      messages:{
        identificador_suc:{
          required:"Por favor ingrese el número de identificacion",
          minlength:"Debe tener mínimo 10 digitos",
          maxlength:"Debe tener máximo 10 digitos",
          digits:"Solo acepta números"
        },
        nombre_suc:{
          required:"por favor ingrese el apellido",
          letras:"solo se acepta letras"
        },
        encargado_suc:{
          required:"por favor ingrese el nombre",
          letras:"solo se acepta letras"
        },
        direccion_suc:{
          required:"por favor ingrese una direccion",
          letras:"solo se acepta letras"
        },
        estado_suc:{
          required:"por favor seleccione un estado"
        }
      }
    });
</script>
