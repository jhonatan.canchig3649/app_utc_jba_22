<?php

    class Camion extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
        return $this->db->insert('camion',$datos);
      }
      public function actualizar($id_cam,$datos){
        $this->db->where("id_cam",$id_cam);
        return $this->db->update("camion",$datos);
      }
      //funcion para sacar el detalle del continente
      public function consultarPorId($id_cam){
        $this->db->where("id_cam",$id_cam);
        $camion=$this->db->get('camion');
        if($camion->num_rows()>0){
          //cuando hay continente
          return $camion->row();
        }else{
          //Cuando no hay continente
          return false;
        }
      }
      //funcion para consultar todos los clientes
      public function consultarTodos(){
        $listadoCamiones=$this->db->get('camion');
        if($listadoCamiones->num_rows()>0){
          //cuando hay clientes
          return $listadoCamiones;
        }else{
          //Cuando no hay clientes
          return false;
        }
      }
    public function eliminar($id_cam){
      $this->db->where("id_cam",$id_cam);
      return $this->db->delete("camion");
    }

    }

?>
