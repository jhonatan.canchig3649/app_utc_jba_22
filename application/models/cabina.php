<?php

    class Cabina extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
        return $this->db->insert('cabina',$datos);
      }
      public function actualizar($id_cabina,$datos){
        $this->db->where("id_cabina",$id_cabina);
        return $this->db->update("cabina",$datos);
      }
      //funcion para sacar el detalle del continente
      public function consultarPorId($id_cabina){
        $this->db->where("id_cabina",$id_cabina);
        $cabina=$this->db->get('cabina');
        if($cabina->num_rows()>0){
          //cuando hay continente
          return $cabina->row();
        }else{
          //Cuando no hay continente
          return false;
        }
      }
      //funcion para consultar todos los clientes
      public function consultarTodos(){
        $listadoCabinas=$this->db->get('cabina');
        if($listadoCabinas->num_rows()>0){
          //cuando hay clientes
          return $listadoCabinas;
        }else{
          //Cuando no hay clientes
          return false;
        }
      }
    public function eliminar($id_cabina){
      $this->db->where("id_cabina",$id_cabina);
      return $this->db->delete("cabina");
    }

    }

?>
