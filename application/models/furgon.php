<?php

    class Furgon extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
        return $this->db->insert('furgon',$datos);
      }
      public function actualizar($id_fur,$datos){
        $this->db->where("id_fur",$id_fur);
        return $this->db->update("furgon",$datos);
      }
      //funcion para sacar el detalle del continente
      public function consultarPorId($id_fur){
        $this->db->where("id_fur",$id_fur);
        $furgon=$this->db->get('furgon');
        if($furgon->num_rows()>0){
          //cuando hay continente
          return $furgon->row();
        }else{
          //Cuando no hay continente
          return false;
        }
      }
      //funcion para consultar todos los clientes
      public function consultarTodos(){
        $listadoFurgones=$this->db->get('furgon');
        if($listadoFurgones->num_rows()>0){
          //cuando hay clientes
          return $listadoFurgones;
        }else{
          //Cuando no hay clientes
          return false;
        }
      }
    public function eliminar($id_fur){
      $this->db->where("id_fur",$id_fur);
      return $this->db->delete("furgon");
    }

    }

?>
