<?php
//para la base de datos
class Mantenimiento extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('mantenimiento',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoMantenimiento=$this->db->get('mantenimiento');
    //para validar consultar clientes
    if ($listadoMantenimiento->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoMantenimiento;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  public function eliminar($id_man){
    $this->db->where("id_man",$id_man);
    return $this->db->delete("mantenimiento");
  }
}
 ?>
