<?php
  class Sucursal extends CI_Model{
    public function __construct(){
        parent:: __construct();
  }

  public function insertar($datos){
      return $this->db->insert('sucursal',$datos);
  }

    public function consultarTodos(){
      $listadoSucursal=$this->db->get('sucursal');
      if ($listadoSucursal->num_rows()>0) {
        return $listadoSucursal;
      } else {
        return false;
      }
    }

    public function eliminar($id_suc){
        $this->db->where("id_suc",$id_suc);
        return $this->db->delete("sucursal");
    }
            //MODIFIC
    public function consultarId($id_suc){
        $this->db->where("id_suc",$id_suc);
        $listadoSucursal=$this->db->get('sucursal');
        if ($listadoSucursal->num_rows()>0) {
          //clientes
          return $listadoSucursal->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_suc,$datos){
         $this->db->where("id_suc",$id_suc);
         return $this->db->update("sucursal",$datos);
       }
  }

 ?>
