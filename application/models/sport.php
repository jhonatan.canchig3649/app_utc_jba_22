<?php
//para la base de datos
class Sport extends CI_model{
  public function __construct() {
    parent::__construct();
  }
  public function insertar($datos){
//para insertar datos en la base de datos
  return $this->db->insert('sport',$datos);
  }
  public function consultarTodos(){
//para consultar
    $listadoSports=$this->db->get('sport');
    //para validar consultar clientes
    if ($listadoSports->num_rows()>0){
      //para que consulte si hay clientes
      return $listadoSports;
    } else
    {
//al no tener clientes devuelve
      return false;
    }
  }
  //eliminar
  public function eliminar($id_sp){
      $this->db->where("id_sp",$id_sp);
      return $this->db->delete("sport");
  }

   //para hacer la consulta de la bdb en la vista
   public function consultarId($id_sp){
       $this->db->where("id_sp",$id_sp);
       $listadoSports=$this->db->get('sport');
       if ($listadoSports->num_rows()>0) {
         //clientes
         return $listadoSports->row();
       }else {
         // sin datos
         return false;
       }
     }
     //actualizar
          public function actualizar($id_sp,$datos){
            $this->db->where("id_sp",$id_sp);
            return $this->db->update("sport",$datos);
          }






}
 ?>
